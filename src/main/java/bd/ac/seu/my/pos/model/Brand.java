package bd.ac.seu.my.pos.model;

/**
 * @author rakib on 12/24/17
 * @project pos
 */

public enum Brand {

    SAMSUNG("SAMSUNG"),
    NOKIA("Nokia"),
    APPLE("Apple"),
    WALTON("Walton"),
    SYMPHONY("Symphony"),
    HUAWEI("Huawei"),
    OPPO("Oppo"),
    LG("Lg"),
    BUTTERFLY("Butterfly"),
    SONNY("Sony"),
    MITSUBISHI("Mitsubishi"),
    HTC("Htc"),
    MOTOROLLA("Motorolla"),
    MICROSOFT("microsoft"),
    XIAOMI("Xaiomi"),
    ASUS("Asus"),
    HP("Hp"),
    LENOVO("Lenovo"),
    BLACKBERRY("Blackberry"),
    LAVA("lava"),
    ACER("Acer"),
    DELL("Dell"),
    FUJITSU("Fujitsu"),
    TOSHIBA("Toshiba");

    private final String Brand;

    Brand(String brand) {
        Brand = brand;
    }

    public String getBrand() {
        return Brand;
    }


}
