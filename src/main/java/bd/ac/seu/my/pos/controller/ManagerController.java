package bd.ac.seu.my.pos.controller;


import bd.ac.seu.my.pos.model.*;
import bd.ac.seu.my.pos.repository.CategoryDao;
import bd.ac.seu.my.pos.repository.OrderDetailsDao;
import bd.ac.seu.my.pos.repository.ProductDao;
import bd.ac.seu.my.pos.repository.UserDetailsDao;
import bd.ac.seu.my.pos.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ManagerController {

    UserDetails userDetails1;
    @Autowired
    ProductDao productDao;

    @Autowired
    OrderDetailsDao orderDetailsDao;

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    UserDetailsDao userDetailsDao;

    String temp;
    Integer userTemp;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String userLogin(Model model){

        model.addAttribute(new UserDetails());
        return "managerLogin";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public String userLogins(@ModelAttribute @Valid UserDetails userDetails, Model model){


        UserDetails userDetails2 = userDetailsDao.findByRole(Role.MANAGER);
        if (userDetails.getEmail().equals(userDetails2.getEmail()) &&
                userDetails.getPassword().equals(userDetails2.getPassword())) {
            userDetailsDao.findByEmailAndPassword(userDetails.getEmail(), userDetails.getPassword());

            userDetails1 = userDetailsDao.findByEmail(userDetails.getEmail());
            model.addAttribute("name",userDetails1.getUserName());
            System.out.println(userDetails1.getUserName());
            return "admin";
        }else
        {
            return "managerLogin";
        }
    }

    @RequestMapping(value = "/tables", method = RequestMethod.GET)
    public String table(Model model) {


        model.addAttribute(new Product());
        model.addAttribute("name",userDetails1.getUserName());

        model.addAttribute("productList", productDao.findAll());
        model.addAttribute("categoryList", categoryDao.findAll());
        model.addAttribute("supplierName",userDetailsDao.findAllByRole(Role.SUPPLIER));
        model.addAttribute("brand", Brand.values());

        return "productTable";
    }

    @RequestMapping(value = "/tables", method = RequestMethod.POST)
    public String tableAddData(@ModelAttribute @Valid Product product, Model model,
                               @RequestParam int categoryId,
                               @RequestParam int supplierId,
                               @RequestParam(required = false) boolean checkbox) {

        product.setIs_active(checkbox);
        Category category = categoryDao.findOne(categoryId);
        UserDetails userDetails = userDetailsDao.findOne(supplierId);
        product.setCategory(category);
        product.setUserDetails(userDetails);

        productDao.save(product);

        model.addAttribute("name",userDetails1.getUserName());

        model.addAttribute("productList", productDao.findAll());
        model.addAttribute("categoryList", categoryDao.findAll());
        model.addAttribute("brand", Brand.values());
        return "productTable";
    }

    @RequestMapping(value = "/editProduct", method = RequestMethod.GET)
    public String editProduct(Model model, @RequestParam(required = false) String id) {


        temp = id;
        model.addAttribute(new Product());
        model.addAttribute("name",userDetails1.getUserName());

        model.addAttribute("singleProduct", productDao.findOne(id));
        model.addAttribute("supplierName",userDetailsDao.findAllByRole(Role.SUPPLIER));
        model.addAttribute("categoryList", categoryDao.findAll());
        model.addAttribute("brand", Brand.values());
        return "editProduct";
    }

    @RequestMapping(value = "/editProduct", method = RequestMethod.POST)

    public String tablePost(@ModelAttribute @Valid Product product, Model model,
                            @RequestParam int categoryId,
                            @RequestParam int supplierId,
                            @RequestParam(required = false) boolean checkbox) {


        product.setIs_active(checkbox);
        Product produc = productDao.findOne(temp);
        Category category = categoryDao.findOne(categoryId);
        UserDetails userDetails = userDetailsDao.findOne(supplierId);
        product.setProductCode(produc.getProductCode());
        product.setCategory(category);
        product.setUserDetails(userDetails);

        productDao.save(product);

        return "redirect:tables";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String delete(@RequestParam String id) {

        productDao.delete(id);
        return "redirect:tables";
    }

    @RequestMapping(value = "/manageUser",method = RequestMethod.GET)
    public String manageUser(Model model){

        model.addAttribute("name",userDetails1.getUserName());

        model.addAttribute("userList",userDetailsDao.findAll());
        model.addAttribute("role", Role.values());
        return "userTable";
    }

    @RequestMapping(value = "/manageUser",method = RequestMethod.POST)
    public String addUser(@ModelAttribute @Valid UserDetails userDetails, Model model,
                          @RequestParam(required = false) boolean checkbox){

        userDetails.set_enable(checkbox);
        userDetailsDao.save(userDetails);
        model.addAttribute("name",userDetails1.getUserName());

        model.addAttribute("userList",userDetailsDao.findAll());
        model.addAttribute("role", Role.values());
        return "userTable";
    }


    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public String editUser(Model model, @RequestParam(required = false) Integer id) {

        userTemp = id;
        model.addAttribute(new UserDetails());
        model.addAttribute("name",userDetails1.getUserName());

        model.addAttribute("singleUser", userDetailsDao.findOne(id));
        model.addAttribute("role", Role.values());
        return "editUser";
    }

    @RequestMapping(value = "/editUser", method = RequestMethod.POST)

    public String editUserPost(@ModelAttribute @Valid UserDetails userDetails,
                            @RequestParam(required = false) boolean checkbox) {


        userDetails.set_enable(checkbox);
        UserDetails details = userDetailsDao.findOne(userTemp);
        userDetails.setId(details.getId());

        userDetailsDao.save(userDetails);

        return "redirect:manageUser";
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public String deleteUser(@RequestParam Integer id) {

        userDetailsDao.delete(id);
        return "redirect:manageUser";
    }

    @RequestMapping(value = "/manageOrder", method = RequestMethod.GET)
    public String manageOrder(Model model){

        List<OrderDetails> orderDetailsList = orderDetailsDao.findAll();

        model.addAttribute("name",userDetails1.getUserName());

        model.addAttribute("orderList",orderDetailsList);
        return "orderTable";
    }

    @RequestMapping(value = "/manageOrder", method = RequestMethod.POST)
    public String deleteOrder(@RequestParam int id,Model model){

        orderDetailsDao.delete(id);
        List<OrderDetails> orderDetailsList = orderDetailsDao.findAll();

        model.addAttribute("name",userDetails1.getUserName());

        model.addAttribute("orderList",orderDetailsList);
        return "orderTable";
    }





}
