package bd.ac.seu.my.pos.repository;

import bd.ac.seu.my.pos.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressDao extends JpaRepository<Address, Integer> {
}
