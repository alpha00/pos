package bd.ac.seu.my.pos.model;

import lombok.Data;

import javax.persistence.*;

/**
 * @author rakib on 12/24/17
 * @project pos
 */

@Entity
public @Data class Address {

    @Id
    @GeneratedValue
    private int addressId;
    @ManyToOne
    @JoinColumn(name = "userId")
    private UserDetails userDetails;

    @Embedded
    private PermanentAddress permanentAddress;

    @Embedded
    private PresentAddress presentAddress;

    private boolean is_billing;
    private boolean is_shipping;
}
