package bd.ac.seu.my.pos.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Blob;
import java.util.List;

/**
 * @author rakib on 12/24/17
 * @project pos
 */

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Product {

    @Id
    private String productCode;
    private String productName;
    private String productDescription;
    @Enumerated(EnumType.STRING)
    private Brand brand;
    private int unitPrice;
    private int quentity;
    private boolean is_active;
    @Lob
    private byte[] productImage;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "supplierId")
    private UserDetails userDetails;

    @OneToMany
    @JoinColumn(name = "productCode")
    private List<OrderDetails> orderDetailsList;


    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public boolean isIs_active() {
        return is_active;
    }
}
