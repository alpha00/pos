package bd.ac.seu.my.pos.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class OrderDetails {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "userId")
    private UserDetails userDetails;

    @ManyToOne
    @JoinColumn(name = "productCode")
    private Product product;

    private int quentity;

    public OrderDetails(UserDetails userDetails, Product product, int quentity) {
        this.userDetails = userDetails;
        this.product = product;
        this.quentity = quentity;
    }
}
