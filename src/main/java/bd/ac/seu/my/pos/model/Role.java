package bd.ac.seu.my.pos.model;

/**
 * @author rakib on 12/30/17
 * @project pos
 */

public enum  Role {

    MANAGER("Manager"),
    SUPPLIER("Supplier"),
    SALESMAN("Salesman"),
    CUSTOMER("Customer");

    private final String Role;

    Role(String role) {
        Role = role;
    }

    public String getRole() {
        return Role;
    }
}
