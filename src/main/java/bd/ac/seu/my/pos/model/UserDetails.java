package bd.ac.seu.my.pos.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author rakib on 12/24/17
 * @project pos
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetails {

    @Id
    @NotNull
    @GeneratedValue
    private int id;
    @Lob
    private byte[] userImage;

    @Column(nullable = false)
    private String userName;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;


    @Size(min=4, max=10, message="Password should be between 4 - 10 charactes")
    @Column(unique = false, nullable = false)
    private String password;


    @Email
    @Column(unique = true, nullable = false)
    private String email;


    @Column(unique = true, nullable = false)
    private String contactNumber;

    private boolean is_enable;

    @OneToMany
    @JoinColumn(name = "supplierId")
    private List<Product> productList;


    @OneToMany
    @JoinColumn(name = "userId")
    private List<OrderDetails> orderDetailsList;


}
