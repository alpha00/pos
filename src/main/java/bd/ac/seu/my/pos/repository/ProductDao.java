package bd.ac.seu.my.pos.repository;

import bd.ac.seu.my.pos.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductDao extends JpaRepository<Product, String> {
    List<Product>findByCategoryCategoryId(int id);
}
