package bd.ac.seu.my.pos.model;

import lombok.Data;

import javax.persistence.Embeddable;

/**
 * @author rakib on 12/24/17
 * @project pos
 */

@Embeddable
public @Data class PresentAddress {

    private String presentCity;
    private String presentState;
    private String presentCountry;
    private String presentPostalCode;

}
