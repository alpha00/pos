package bd.ac.seu.my.pos.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Blob;
import java.util.List;

/**
 * @author rakib on 12/24/17
 * @project pos
 */

@Entity
@Data
public class Category {

    @Id
    @GeneratedValue
    private int categoryId;
    private String categoryName;
    private String description;
    private Blob image;
    private boolean is_active;

    @OneToMany
    @JoinColumn(name = "categoryId")
    private List<Product> productList;
}
