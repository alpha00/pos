package bd.ac.seu.my.pos.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.Lob;

@Service
@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrderService {

    @Lob
    private byte[] productImage;
    private int userId;
    private String productCode;
    private String productName;
    private int quentity;
    private int unitPrice;
    private int totalPrice;

}
