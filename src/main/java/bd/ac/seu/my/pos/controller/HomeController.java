package bd.ac.seu.my.pos.controller;

import bd.ac.seu.my.pos.model.Category;
import bd.ac.seu.my.pos.model.Product;
import bd.ac.seu.my.pos.repository.CategoryDao;
import bd.ac.seu.my.pos.repository.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
public class HomeController {

    @Autowired
    private ProductDao productDao;

    @Autowired
    private CategoryDao categoryDao;

    @RequestMapping(value = "/")
    private String home(Model model){




        model.addAttribute("allProduct",productDao.findAll());
        model.addAttribute("allCategory",categoryDao.findAll());
        return "index";
    }

    @RequestMapping(value = "/category" )
    private String category(Model model, @RequestParam int id){
        List<Product>productList;
        productList = productDao.findByCategoryCategoryId(id);
        model.addAttribute("allProduct",productList);
        model.addAttribute("allCategory",categoryDao.findAll());
        return "index";
    }
}
