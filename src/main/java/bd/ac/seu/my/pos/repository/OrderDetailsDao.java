package bd.ac.seu.my.pos.repository;

import bd.ac.seu.my.pos.model.OrderDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDetailsDao extends JpaRepository<OrderDetails,Integer>{
}
