package bd.ac.seu.my.pos.repository;

import bd.ac.seu.my.pos.model.Role;
import bd.ac.seu.my.pos.model.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDetailsDao extends JpaRepository<UserDetails, Integer> {
    List<UserDetails>findAllByRole(Role role);
    UserDetails findByRole(Role role);
    UserDetails findByEmailAndPassword(String email,String password);
    UserDetails findByEmail(String email);
}
