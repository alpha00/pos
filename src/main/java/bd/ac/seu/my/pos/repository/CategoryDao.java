package bd.ac.seu.my.pos.repository;

import bd.ac.seu.my.pos.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryDao extends JpaRepository<Category,Integer>{
}
