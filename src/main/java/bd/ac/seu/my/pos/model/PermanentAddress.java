package bd.ac.seu.my.pos.model;

import lombok.Data;

import javax.persistence.Embeddable;

/**
 * @author rakib on 12/24/17
 * @project pos
 */

@Embeddable
public @Data class PermanentAddress {

    private String permanentCity;
    private String permanentState;
    private String permanentCountry;
    private String permanentPostalCode;

}
