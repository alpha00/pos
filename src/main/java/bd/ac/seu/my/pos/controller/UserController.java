package bd.ac.seu.my.pos.controller;

import bd.ac.seu.my.pos.model.OrderDetails;
import bd.ac.seu.my.pos.model.Product;
import bd.ac.seu.my.pos.model.UserDetails;
import bd.ac.seu.my.pos.repository.*;
import bd.ac.seu.my.pos.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {

    int userid;
    int totalPrice = 0;
    List<OrderService> orderServiceList = new ArrayList<>();
    @Autowired
    OrderDetailsDao orderDetailsDao;
    @Autowired
    UserDetailsDao userDetailsDao;
    @Autowired
    ProductDao productDao;
    @Autowired
    CategoryDao categoryDao;


    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    public String userLogin(Model model){

        model.addAttribute(new UserDetails());
        return "userLogin";
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String userLogin(@ModelAttribute @Valid UserDetails userDetails, Model model){

          UserDetails userDetails1 =
                  userDetailsDao.findByEmailAndPassword(userDetails.getEmail(),userDetails.getPassword());

          if (userDetails1 == null){
              return "userLogin";
          }else {
              userid = userDetails1.getId();


              model.addAttribute(new Product());
              model.addAttribute("userName","Welcome "+userDetails1.getUserName());
              model.addAttribute("allProduct",productDao.findAll());
              model.addAttribute("allCategory",categoryDao.findAll());
              return "index";

          }
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String manageOrder(@ModelAttribute @Valid Product product, Model model){

        if (userid == 0){
            return "userLogin";
        }
        UserDetails userDetails = userDetailsDao.findOne(userid);
        Product product1 = productDao.findOne(product.getProductCode());

        orderDetailsDao.save(new OrderDetails(userDetails,product1,product.getQuentity()));

        model.addAttribute("userName","Welcome "+userDetails.getUserName());

        model.addAttribute("allProduct",productDao.findAll());
        model.addAttribute("allCategory",categoryDao.findAll());
        return "index";
    }


}
